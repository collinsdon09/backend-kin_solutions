import socket
import pyaudio

def receive_audio(sock, stream):
    # Receive audio data from the server and play it
    while True:
        chunk = sock.recv(1024)
        if not chunk:
            break
        stream.write(chunk)

def start_client():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(("localhost", 5544))

    # Start PyAudio
    p = pyaudio.PyAudio()

    # Open a stream to play the audio
    stream = p.open(format=pyaudio.paInt16,
                    channels=1,
                    rate=44100,
                    output=True)

    # Receive and play the audio
    receive_audio(client_socket, stream)

if __name__ == "__main__":
    start_client()
