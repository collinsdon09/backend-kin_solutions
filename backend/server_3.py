import socket
import pyaudio
import wave

def send_audio(sock, chunk):
    # Send audio data to the client
    sock.sendall(chunk)

def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("", 5544))
    server_socket.listen(1)

    print("Waiting for a client to connect...")
    conn, address = server_socket.accept()
    print("Client connected from", address)

    # Start PyAudio
    p = pyaudio.PyAudio()

    # Open a stream for the microphone
    stream = p.open(format=pyaudio.paInt16,
                    channels=1,
                    rate=44100,
                    input=True,
                    frames_per_buffer=1024)

    # Stream the audio to the client
    while True:
        chunk = stream.read(1024)
        send_audio(conn, chunk)

if __name__ == "__main__":
    start_server()
